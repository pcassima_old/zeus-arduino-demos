
const uint8_t SER = 6;
const uint8_t OE = 5;
const uint8_t RCLK = 4;
const uint8_t SRCLK = 3;
const uint8_t CLR = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(SER, OUTPUT);
  pinMode(OE, OUTPUT);
  pinMode(RCLK, OUTPUT);
  pinMode(SRCLK, OUTPUT);
  pinMode(CLR, OUTPUT);
  digitalWrite(CLR, HIGH);
}

void shiftOutByte(uint8_t data) {

  digitalWrite(CLR, LOW);
  delay(1);
  digitalWrite(CLR, HIGH);
  delay(1);
  for (int i = 0; i < 8; i++) {
    digitalWrite(SER, (data >> i) & 0b1);
    digitalWrite(SRCLK, HIGH);
    digitalWrite(SRCLK, LOW);
  }
  delay(1);
  digitalWrite(RCLK, HIGH);
  digitalWrite(RCLK, LOW);

  //  digitalWrite(OE, LOW);

}

uint8_t counter = 0;

void loop() {
  // put your main code here, to run repeatedly:

  shiftOutByte(counter);
  delay(200);
  counter++;
  counter = counter > 127 ? 0 : counter;
}
