/*
   Chronos shield demo
   Pieter-Jan Cassiman
*/

/*
   PINMAP:
   HC5959:
      OE    D10
      RCLK  D11
      SRCLK D12
      CLR   D13
      SER   D9
*/

const uint8_t SER = 9;
const uint8_t OE = 10;
const uint8_t RCLK = 11;
const uint8_t SRCLK = 12;
const uint8_t CLR = 13;

const uint8_t dSel0 = 8;
const uint8_t dSel1 = 7;

void shiftOutByte(uint8_t data) {
  digitalWrite(CLR, LOW);
  digitalWrite(CLR, HIGH);
  for (int i = 0; i < 8; i++) {
    digitalWrite(SER, (data >> i) & 0b1);
    digitalWrite(SRCLK, HIGH);
    digitalWrite(SRCLK, LOW);
  }
  digitalWrite(RCLK, HIGH);
  digitalWrite(RCLK, LOW);
}
void setup() {
  for (int i = 9; i <= 13; i++) {
    pinMode(i, OUTPUT);
  }
  digitalWrite(CLR, HIGH);

  pinMode(dSel0, OUTPUT);
  pinMode(dSel1, OUTPUT);
}

void writeDigit(uint8_t segments, uint8_t digit) {
  shiftOutByte(segments);
  digitalWrite(dSel0, ~digit & 0b1);
  digitalWrite(dSel1, ~(digit >> 1) & 0b1);
  delay(2);
}

uint8_t display = 0b00000000;
uint8_t state = 0;
uint8_t digit = 0;
const uint8_t stateDelay = 100;
unsigned long previousMillis = 0;

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= stateDelay){
    previousMillis = currentMillis;
    if (state <= 3){
      display = 0b00010000;
      digit = state;
    } else if (state==4){
      display = 0b00001000;
      digit = 3;
    } else if (state==5){
      display = 0;
      display = 0b00000100;
      digit = 3;
    } else if (state <= 9) {
      display = 0;
      display = 0b10000000;
      digit = 3-((state-2)%4);
    } else if (state==10){
      display = 0b01000000;
      digit = 0;
    } else if (state==11){
      display = 0b00100000;
      digit = 0;
    } else {
      // fail safe
      display = 0;
      digit = 0;
    }
    state = (state+1)%12;
  }
 // put your main code here, to run repeatedly:
 writeDigit(display, digit);
}
